import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { swapi } from 'src/environments/environment';

@Injectable()
export class SwapiService {
  private baseUrl: string = swapi.baseURL;
  constructor(private _http: HttpClient) {}

  public getCharacterData(id: number = 9): Observable<any> {
    return this._http.get(`${this.baseUrl}${swapi.people}${id}/`);
  }
}
