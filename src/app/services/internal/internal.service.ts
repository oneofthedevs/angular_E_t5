import { Injectable } from '@angular/core';
import { interval, Observable } from 'rxjs';

@Injectable()
export class InternalService {
  public oneSecInterval: Observable<number>;
  constructor() {
    this.oneSecInterval = interval(1000);
  }
}
