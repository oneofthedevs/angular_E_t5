import { InjectionToken } from '@angular/core';
import { BoredApiService } from '../services/boredApi/bored-api.service';

export const boredApiProvider = new InjectionToken<BoredApiService>(
  'BoredApiService'
);
