import { Component, Inject, OnInit, Self } from '@angular/core';
import { boredApiProvider } from 'src/app/extra/InjectionToken.class';
import { BoredApiService } from 'src/app/services/boredApi/bored-api.service';
import { InternalService } from 'src/app/services/internal/internal.service';
import { SwapiService } from 'src/app/services/swapi/swapi.service';

@Component({
  selector: 'parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss'],
  providers: [SwapiService, InternalService],
})
export class ParentComponent implements OnInit {
  public intervalValue: string = '0';
  public task: string | undefined;
  constructor(
    @Inject(boredApiProvider) private _bored: BoredApiService,
    @Self() private _internal: InternalService
  ) {}

  ngOnInit(): void {
    this.getTask();
    this.getIntervalValue();
  }

  private getTask(): void {
    this._bored.getRandomIdea().subscribe((res) => (this.task = res));
  }

  private getIntervalValue(): void {
    this._internal.oneSecInterval.subscribe(
      (res) => (this.intervalValue = res.toString())
    );
  }
}
